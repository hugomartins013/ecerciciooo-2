package Banco;

public class Transacao {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Conta c = new Conta();
		ContaCorrente cc = new ContaCorrente();
		ContaPoupanca cp = new ContaPoupanca();
		
		c.deposita(1000);
		cc.deposita(1000);
		cp.deposita(1000);
		
		c.atualiza(0.01);
		cc.atualiza(0.01);
		cp.atualiza(0.01);
		
		System.out.println(c.getSaldo());
		System.out.println(cc.getSaldo());
		System.out.println(cp.getSaldo());
	}
/* Após o programa ser executado, aparece 1000,00 no saldo ca conta, 
 * 1019,898 no saldo da conta corrente e 1030,0 no saldo da conta poupança. 
 * O programa dá erro se executarmos da maneira proposta na questão de número 
 * 5 do capítulo 7 da apostila, pois apesar das contas terem sido criadas, não há 
 * nenhum método sendo executado nelas.*/
}
